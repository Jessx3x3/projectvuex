import Vue from 'vue'
import Vuex from '../views/Vuex.vue'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        //Variable "globales".
        compras: 0,
        poblacion: 0,
        area: 0,
        paises: [],
        likes: 0
    },
    getters: {
        /*Este Getter calcula una nuevo dato a partir
        de las variables establecidas.*/
        promedio(state) {
            if (state.compras === 0) {
                return 0
            }
            return parseInt(state.poblacion / state.compras)
        }
    },
    mutations: {
        nuevaCompra(state, precio) {
            state.compras = state.compras + 1
            state.poblacion = state.poblacion + precio
        },
        agregarArea(state, area) {
            state.area = state.area + area
        },
        getPaises(state, paises) {
            state.paises = paises
        },
        setLikes(state, like) {
            if (like) state.likes++
            else state.likes--
        }
    },
    actions: {

        //Metodo Asincronico.
        async getAllCountries(context) {
            return await axios.get('https://restcountries.eu/rest/v2/regionalbloc/usan').then(
                response => {
                    let paises = response.data
                    context.commit('getPaises', paises)
                })
        }
    }
})
